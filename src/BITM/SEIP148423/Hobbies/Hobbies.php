<?php

namespace App\Hobbies;

use App\Model\Database as DB;


class Hobbies extends DB{

    public $id="01";
    public $name="Afsana";
    public $hobbies="Sleeping, Gardening, Searching, Collecting new things";

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo $this->id."<br>";
        echo $this->name."<br>";
        echo $this->hobbies."<br>";

    }

}


//$objHobbies = new Hobbies();