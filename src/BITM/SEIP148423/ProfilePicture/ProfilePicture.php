<?php

namespace App\ProfilePicture;
use App\Model\Database as DB;


class ProfilePicture extends DB{

    public $id="01";
    public $name="Steve Jobs";
    public $profilePicture="Stevejobs.jpg";

    public function __construct()
    {
        parent::__construct();
    }


    public function index(){
        echo $this->id."<br>";
        echo $this->name."<br>";
        echo $this->profilePicture."<br>";
    }
}


//$objBooktitle = new ProfilePicture();