<?php

namespace App\SummaryOfOrganization;

use App\Model\Database as DB;


class SummaryOfOrganization extends DB{

    public $id="01";
    public $name="WHO";
    public $organization="World Health Organaization";

    public function __construct()
    {
        parent::__construct();
    }


    public function index(){
        echo $this->id."<br>";
        echo $this->name."<br>";
        echo $this->organization."<br>";
    }
}


//$objSummaryOfOrganization = new SummaryOfOrganization();