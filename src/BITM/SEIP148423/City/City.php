<?php

namespace App\City;

use App\Model\Database as DB;


class City extends DB{

    public $id="01";
    public $name="Afsana";
    public $city="New York";
    public $country="USA";

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){

        echo $this->id."<br>";
        echo $this->name."<br>";
        echo $this->city."<br>";
        echo $this->country."<br>";
    }

}

//$objCity = new City();