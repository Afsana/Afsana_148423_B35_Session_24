<?php

namespace App\Gender;

use App\Model\Database as DB;


class Gender extends DB{

    public $id="01";
    public $name="Afsana";
    public $gender="Female";

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo $this->id."<br>";
        echo $this->name."<br>";
        echo $this->gender."<br>";
    }

}


//$objGender = new Gender();