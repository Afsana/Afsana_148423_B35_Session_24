<?php

namespace App\Birthday;

use App\Model\Database as DB;


class Birthday extends DB{

    public $id="01";
    public $name="Afsana";
    public $birthDate="2000/10/20";

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "<br>".$this->id."<br>";
        echo $this->name."<br>";
        echo $this->birthDate."<br>";

    }

}

//$objBirthday = new Birthday();